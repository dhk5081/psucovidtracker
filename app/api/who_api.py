#!/usr/bin/env python

# make sure to install these packages before running:
# pip install pandas

import pandas as pd


class who_data:
    def __init__(self):
        self.link = 'https://covid19.who.int/WHO-COVID-19-global-data.csv'

    def who_data_pull(self):
        # Read in csv
        df = pd.read_csv(self.link)

        # Convert to pandas DataFrame and trim column labels
        df.columns = df.columns.to_series().apply(lambda x: x.strip())

        # Retrieve last occurrence of country info from datafframe
        df_1 = (df.groupby('Country').tail(1))
        print(df_1[['Country', 'New_cases', 'New_deaths']])
        return

# foo = who_data()
# foo.who_data_pull()
