#!/usr/bin/env python

# make sure to install these packages before running:
# pip install pandas
# pip install sodapy

import pandas as pd
from sodapy import Socrata


def get_cdc_data():
    # Unauthenticated client only works with public data sets. Note 'None'
    # in place of application token, and no username or password:
    client = Socrata("data.cdc.gov", None)

    # Example authenticated client (needed for non-public datasets):
    # client = Socrata(data.cdc.gov,
    #                  MyAppToken,
    #                  userame="user@example.com",
    #                  password="AFakePassword")

    # Return COVID-19 Data about cases and deaths by a specific date,
    # for all US states.
    # Sources:
    # https://dev.socrata.com/foundry/data.cdc.gov/9mfq-cb36
    # http://api.us.socrata.com/api/catalog/v1?ids=9mfq-cb36
    # https://github.com/xmunoz/sodapy#getdataset_identifier-content_typejson-kwargs
    # https://dev.socrata.com/docs/queries/

    month = ["01", "02", "03", "04", "05", "06",
             "07", "08", "09", "10", "11", "12"]
    day = "01"
    dataDate = "2020-" + month[8] + "-" + day + "T00:00:00.000"
    # submission_date example : "2020-09-18T00:00:00.000"
    results = client.get("9mfq-cb36", submission_date=dataDate,
                         select="state, new_case, new_death")

    # Convert to pandas DataFrame
    results_df = pd.DataFrame.from_records(results)
    # print(results_df.to_string())
    # more options can be specified also
    # with pd.option_context('display.max_rows', None,
    # 'display.max_columns', None):
    # print(results_df)

    client.close()

    return results_df
