from flask import Flask
from flask import render_template, request
from flask_json import FlaskJSON, JsonError, json_response, as_json
import datetime
import secrets
import app.api.cdc_api as cdc

app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = secrets.token_urlsafe()
json = FlaskJSON(app)


# Use decorator to tell Flask to correlate get_state_data function
# with specified URL
@app.route('/<string:state>', methods=['GET'])
def get_state_data(state):
    """
    render the US State COVID data page to the user
    :return: HTML webpage to user
    """
    print("\t", datetime.datetime.now(), request.remote_addr,
          "USState",  state)
    return render_template(r'states.html', USState=state,
                           timestamp=datetime.datetime.now())


# Use decorator to tell Flask to correlate index function with specified URL
@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    """
    render the homepage to the user
    :return: HTML webpage to user
    """
    # Print data to console for logging
    print("\t", datetime.datetime.now(), request.remote_addr, "INDEX")
    # Return search form to user
    return render_template(r'index.html', timestamp=datetime.datetime.now())


@app.route('/api/cdc', methods=['GET'])
@as_json
def get_cdc_api():
    return json_response(data=cdc.get_cdc_data().to_json())
