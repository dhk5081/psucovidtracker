import numpy as np
import pandas as pd

from ..api import cdc_api


# submission_date used to produce results : "2020-09-18T00:00:00.000"
def example_cdc_data():
    test_data = [
        {'state': 'CO', 'new_case': '351.0', 'new_death': '1.0'},
        {'state': 'FL', 'new_case': '7487.0', 'new_death': '187.0'},
        {'state': 'AZ', 'new_case': '507.0', 'new_death': '15.0'},
        {'state': 'SC', 'new_case': '854.0', 'new_death': '37.0'},
        {'state': 'CT', 'new_case': '127.0', 'new_death': '1.0'},
        {'state': 'NE', 'new_case': '287.0', 'new_death': '2.0'},
        {'state': 'KY', 'new_case': '789.0', 'new_death': '15.0'},
        {'state': 'WY', 'new_case': '24.0', 'new_death': '4.0'},
        {'state': 'IA', 'new_case': '625.0', 'new_death': '6.0'},
        {'state': 'NM', 'new_case': '108.0', 'new_death': '8.0'},
        {'state': 'ND', 'new_case': '267.0', 'new_death': '3.0'},
        {'state': 'WA', 'new_case': '304.0', 'new_death': '16.0'},
        {'state': 'RMI', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'TN', 'new_case': '1396.0', 'new_death': '27.0'},
        {'state': 'AS', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'MA', 'new_case': '686.0', 'new_death': '4.0'},
        {'state': 'PA', 'new_case': '770.0', 'new_death': '18.0'},
        {'state': 'NYC', 'new_case': '250.0', 'new_death': '5.0'},
        {'state': 'OH', 'new_case': '1453.0', 'new_death': '27.0'},
        {'state': 'AL', 'new_case': '1558.0', 'new_death': '18.0'},
        {'state': 'VA', 'new_case': '927.0', 'new_death': '29.0'},
        {'state': 'MI', 'new_case': '795.0', 'new_death': '14.0'},
        {'state': 'CA', 'new_case': '3712.0', 'new_death': '85.0'},
        {'state': 'NJ', 'new_case': '330.0', 'new_death': '5.0'},
        {'state': 'MS', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'IL', 'new_case': '1492.0', 'new_death': '38.0'},
        {'state': 'TX', 'new_case': '4364.0', 'new_death': '145.0'},
        {'state': 'GA', 'new_case': '2226.0', 'new_death': '100.0'},
        {'state': 'LA', 'new_case': '689.0', 'new_death': '54.0'},
        {'state': 'WI', 'new_case': '1036.0', 'new_death': '9.0'},
        {'state': 'NV', 'new_case': '400.0', 'new_death': '8.0'},
        {'state': 'IN', 'new_case': '695.0', 'new_death': '16.0'},
        {'state': 'PR', 'new_case': '582.0', 'new_death': '8.0'},
        {'state': 'NY', 'new_case': '449.0', 'new_death': '2.0'},
        {'state': 'OR', 'new_case': '233.0', 'new_death': '6.0'},
        {'state': 'MD', 'new_case': '456.0', 'new_death': '5.0'},
        {'state': 'OK', 'new_case': '672.0', 'new_death': '9.0'},
        {'state': 'NC', 'new_case': '2111.0', 'new_death': '39.0'},
        {'state': 'ID', 'new_case': '280.0', 'new_death': '7.0'},
        {'state': 'UT', 'new_case': '358.0', 'new_death': '0.0'},
        {'state': 'AR', 'new_case': '273.0', 'new_death': '17.0'},
        {'state': 'MO', 'new_case': '1058.0', 'new_death': '8.0'},
        {'state': 'DE', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'MN', 'new_case': '1221.0', 'new_death': '16.0'},
        {'state': 'WV', 'new_case': '257.0', 'new_death': '8.0'},
        {'state': 'RI', 'new_case': '53.0', 'new_death': '2.0'},
        {'state': 'DC', 'new_case': '57.0', 'new_death': '0.0'},
        {'state': 'ME', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'KS', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'SD', 'new_case': '240.0', 'new_death': '0.0'},
        {'state': 'NH', 'new_case': '22.0', 'new_death': '0.0'},
        {'state': 'HI', 'new_case': '177.0', 'new_death': '4.0'},
        {'state': 'MT', 'new_case': '196.0', 'new_death': '3.0'},
        {'state': 'AK', 'new_case': '35.0', 'new_death': '2.0'},
        {'state': 'VT', 'new_case': '5.0', 'new_death': '0.0'},
        {'state': 'GU', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'VI', 'new_case': '4.0', 'new_death': '0.0'},
        {'state': 'MP', 'new_case': '1.0', 'new_death': '0.0'},
        {'state': 'FSM', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'PW', 'new_case': '0.0', 'new_death': '0.0'}
    ]
    return pd.DataFrame.from_records(test_data)


# Unit test to ensure API data returned matches what we expect
def test_cdc_api():
    # Arrange
    expected = example_cdc_data().sort_values("state")

    # Act
    results = cdc_api.get_cdc_data().sort_values("state")

    # Assert
    # indexes are different from API call to call, so just look at values
    np.array_equal(results.values, expected.values)
