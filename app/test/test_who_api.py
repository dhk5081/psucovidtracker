import requests

from ..api.who_api import who_data


'''
    # Source:
    # https://www.kite.com/python/answers/how-to-check-if-a-string-is-a-url-in-python
    try:
        response = requests.get("http://www.avalidurl.com/")
        print("URL is valid and exists on the internet")
    except requests.ConnectionError as exception:
        print("URL does not exist on Internet")

    # Goal: Assert the link is still valid
'''


def test_who_api():
    test_case = who_data()

    try:
        response = requests.get(test_case.link)
        pass_test = True
    except requests.ConnectionError as exception:
        pass_test = False

    assert pass_test
    return pass_test
