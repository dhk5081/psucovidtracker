require(['esri/Map',
  'esri/views/MapView',
  'esri/widgets/Search'], function(Map, MapView, Search) {
  // the base map
  const map = new Map({
    basemap: 'dark-gray',
  });

  // the view to be displayed on the html page
  const view = new MapView({
    container: 'viewDiv',
    map: map,
    zoom: 5,
    center: [-90, 35], // longitude, latitude
  });

  // add search bar UI control
  const searchWidget = new Search({view: view});
  view.ui.add(searchWidget, {position: 'top-right'});
});
