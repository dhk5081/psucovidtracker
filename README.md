# Run Development Web Application
* Run `flask run`
* PSU COVID Track will be active at http://127.0.0.1:5000/

# Set Up
## Development Environment
1. If running in Windows, the following also needs to be installed:
    1. Microsoft Windows SDK: https://developer.microsoft.com/en-US/windows/downloads/windows-10-sdk/
    1. Microsoft C++ Build Tools: https://visualstudio.microsoft.com/visual-cpp-build-tools/
1. Install Python & Git on your machine
1. Get the repository from BitBucket: 
    ```bash
    git clone https://[BITBUCKET USERNAME]@bitbucket.org/psusweng894capstone/psucovidtracker.git`
    ```
1. Create local python virtual environment
    1. macOS & Linux:
        ```bash
        python3 -m venv venv
        ```
    1. Windows:
        ```bash
        py -m venv venv
        ```
1. Activate the virtual environment
    1. macOS & Linux:
        ```bash
        source venv/bin/activate
        ```
    1. Windows:
        ```bash
        .\venv\Scripts\activate
        ```
1. Install python packages:
    ```bash
    pip install -r venv_requirements.txt
    ```

## Configure Git Username and Email
You must confgure your git identitfy before making commits to the repository
https://support.atlassian.com/bitbucket-cloud/docs/configure-your-dvcs-username-for-commits/

### Repository-specific username/email configuration:

1. From the command line, change into the repository directory.
1. Set your username:
    ```bash
    git config user.name "FIRST_NAME LAST_NAME"
    ```
1. Set your BitBucket email address:
    ```bash
    git config user.email "MY_NAME@example.com"
    ```
1. Verify your configuration by displaying your configuration file:
    ```bash
    cat .git/config
    ```

### Global username/email configuration:
1. Open the command line.
1. Set your username:
    ```bash
    git config --global user.name "FIRST_NAME LAST_NAME"
    ```
1. Set your BitBucket email address:
    ```bash
    git config --global user.email "MY_NAME@example.com"
    ```
    
## Using Git

### Branching Structure
To select a branch, run git branch [branch name]
master: this branch stores the release candidates to deploy to the main web server
development: this branch stores the active source to be used by developers

developers should create a new branch (preferably through a Jira issue) and not work directly from the development branch